#include<stdio.h>
#include<string.h>
#include <R.h>


void
bluecarmove( int *grid, int *newgrid, const int *dims, int* numbluecars, int *bluecarlocations, int *newvelocity){
	
	int k = 0, i = 0 ,j = 0 ,ni = 0 ,nj = 0;
	int numMoved = 0;
	for (i = 0; i < dims[0] * dims[1]; i++) {
      newgrid[i] = grid[i];
   }
	
	for (k=0; k<numbluecars[0];k++){
		i = bluecarlocations[k] - 1;
		j = bluecarlocations[k + numbluecars[0]] - 1;
		nj = j;
		ni = i+1;
		if(ni == dims[0])
        ni = 0;
	
	if (grid[ni + nj * dims[0]] == 0){
		numMoved++;
        newgrid[ ni + nj * dims[0] ] = 1;
        newgrid[ i + j * dims[0] ] = 0;
		bluecarlocations[k] = ni + 1;
	}
	newvelocity[0] = numMoved;

}
} 