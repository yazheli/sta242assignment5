#1 slower version
#rondom generate red/blue cars position
#1 represent red car move west 2 represnet blue car move south 0 represent no car 
cars_position = function (m, n, n_cars){
  #m number of rows, n number columns, n_cars number of red car and blue car  
  if (m<=0 | m%%1 != 0){
    stop ('row number wrong')
  }
  if (n<=0 | n%%1 != 0){
    stop ('column number wrong')
  }
  
  cars = c(rep(1,floor(n_cars)),rep(2,floor(n_cars)))
  empty=rep(0,(m*n-(2*n_cars)))
  randomplace = sample(c(cars,empty),size = m*n)
  return (randomplace)
}

#acording to cars_position generate car matrix
createBMLGrid = function (m, n, p){
  #m number of rows, n number columns, p density of cars 
  if (m<0 | m%%1 != 0){
    stop ('row number wrong')
  }
  if (n<0 | n%%1 != 0){
    stop ('column number wrong')
  }
  if (p<0 | p>1){
    stop ('car density wrong')
  }
  
  n_cars = floor(m*n*p/2) #obtian the number of red/blue cars
  carposi = cars_position(m, n, n_cars)
  carmatrix = matrix(FALSE, nrow = m, ncol = n)
  
  rx = rep(0,n_cars)
  ry = rep(0,n_cars)
  bx = rep(0,n_cars)
  by = rep(0,n_cars)
  
  i = 1
  j = 1
  k = 1
  
  while(i <= m * n){
    if (carposi[i] == 1){
      x = (i-1) %% m + 1
      y = (i-1) %/% m + 1
      rx[j] = x
      ry[j] = y
      carmatrix[x, y] = TRUE
      j = j + 1
      
    }
    else if(carposi[i] == 2){
      x = (i-1) %% m + 1
      y = (i-1) %/% m + 1
      bx[k] = x 
      by[k] = y
      carmatrix[x, y] = TRUE
      k = k + 1
    }
    i = i + 1
  }
  rposi = cbind(rx, ry)
  bposi = cbind(bx, by)
  car_matrix = list(redcar = rposi, bluecar = bposi, carmatrix = carmatrix,velocity=NA)
  class( car_matrix ) <- append( "BMLGrid",class( car_matrix ))
  return (car_matrix)
}

#car move function, 
move_index = function(x,m){
  return(ifelse (x<=m, x, 1))
}

#red car move
movered = function(redcar, bluecar, carmatrix){
  ncar = nrow(redcar)
  x = redcar[,1]
  y = redcar[,2]
  m = nrow(carmatrix)
  
  n = ncol(carmatrix)
  
  ny = move_index(y + 1, n)
  next_redcar = cbind(x, ny)
  ismoveok = carmatrix[next_redcar]
  for (i in 1:length(ismoveok)) 
  {
    if (ismoveok[i] == TRUE) y[i] = y[i]
    else y[i] = ny[i]
  }
  
  new_car_matrix = matrix(FALSE, nrow = m, ncol = n)
  new_red_car = cbind(x, y)
  new_car_matrix[new_red_car] = TRUE
  new_car_matrix[bluecar] = TRUE
  
  return(list(new_red_car, new_car_matrix))
}

#blue car move
moveblue = function(bluecar, redcar, carmatrix){
  ncar = nrow(bluecar)
  x = bluecar[,1]
  y = bluecar[,2]
  m = nrow(carmatrix)
  n = ncol(carmatrix)
  
  nx = move_index(x + 1, m)
  next_bluecar = cbind(nx, y)
  ismoveok = carmatrix[next_bluecar]
  for (i in 1:length(ismoveok))
  {
    if (ismoveok[i] == TRUE) x[i] = x[i]
    else x[i] = nx[i]
  }
  
  new_car_matrix = matrix(FALSE, nrow = m, ncol = n)
  new_blue_car = cbind(x, y)
  new_car_matrix[new_blue_car] = TRUE
  new_car_matrix[redcar] = TRUE
  
  return(list(new_blue_car, new_car_matrix))
}


#finish one step move by move red car one time and move blue car one time
onetimemove = function(car_matrix){
  
  old_car_matrix = car_matrix
  new_red_position = movered(car_matrix$redcar, car_matrix$bluecar, car_matrix$carmatrix)
  car_matrix$redcar = new_red_position[[1]]
  car_matrix$carmatrix = new_red_position[[2]]
  
  new_blue_position = moveblue(car_matrix$bluecar, car_matrix$redcar, car_matrix$carmatrix)
  car_matrix$bluecar = new_blue_position[[1]]
  car_matrix$carmatrix = new_blue_position[[2]]
  car_matrix$velocity = sum((car_matrix$carmatrix - old_car_matrix$carmatrix)>0)/(nrow(car_matrix$bluecar)
                                                                                  +nrow(car_matrix$redcar))
  return(car_matrix)
}

runBMLGrid = function(BMLGrid,numSteps) 
{ 
  if (numSteps<0 | numSteps%%1 != 0){
    stop ('wrong number of steps')
  }
  
  for( i in 1:numSteps){
    BMLGrid = onetimemove(BMLGrid)}
  return (BMLGrid)
}

#2 Faster version
#rondom generate red/blue cars position
#1 represent red car move west 2 represnet blue car move south 0 represent no car 
cars_position = function (m, n, n_cars){
  #m number of rows, n number columns, n_cars number of red car and blue car  
  if (m<=0 | m%%1 != 0){
    stop ('row number wrong')
  }
  if (n<=0 | n%%1 != 0){
    stop ('column number wrong')
  }
  
  cars = c(rep(1,floor(n_cars)),rep(2,floor(n_cars)))
  empty=rep(0,(m*n-(2*n_cars)))
  randomplace = sample(c(cars,empty),size = m*n)
  return (randomplace)
}

#acording to cars_position generate car matrix
createBMLGrid = function (m, n, p){
  #m number of rows, n number columns, p density of cars 
  if (m<=0 | m%%1 != 0){
    stop ('row number wrong')
  }
  if (n<=0 | n%%1 != 0){
    stop ('column number wrong')
  }
  if (p<0 | p>1){
    stop ('car density wrong')
  }
  
  n_cars = floor(m*n*p/2) #obtian the number of red/blue cars
  carposi = cars_position(m, n, n_cars)
  carmatrix = matrix(FALSE, nrow = m, ncol = n)
  
  rx = rep(0,n_cars)
  ry = rep(0,n_cars)
  bx = rep(0,n_cars)
  by = rep(0,n_cars)
  
  i = 1
  j = 1
  k = 1
  
  while(i <= m * n){
    if (carposi[i] == 1){
      x = (i-1) %% m + 1
      y = (i-1) %/% m + 1
      rx[j] = x
      ry[j] = y
      carmatrix[x, y] = TRUE
      j = j + 1
      
    }
    else if(carposi[i] == 2){
      x = (i-1) %% m + 1
      y = (i-1) %/% m + 1
      bx[k] = x 
      by[k] = y
      carmatrix[x, y] = TRUE
      k = k + 1
    }
    i = i + 1
  }
  rposi = cbind(rx, ry)
  bposi = cbind(bx, by)
  car_matrix = list(redcar = rposi, bluecar = bposi, carmatrix = carmatrix,velocity=NA)
  class( car_matrix ) <- append( "BMLGrid",class( car_matrix ))
  return (car_matrix)
}

#car move function, 
move_index = function(x,m){
  return(ifelse (x<=m, x, 1))
}

#red car move
movered = function(redcar, bluecar, carmatrix){
  ncar = nrow(redcar)
  x = redcar[,1]
  y = redcar[,2]
  m = nrow(carmatrix)
  n = ncol(carmatrix)
  ny = move_index(y + 1, n)
  next_redcar = cbind(x, ny)
  ismoveok = carmatrix[next_redcar]
  y = ifelse(ismoveok, y, ny)
  
  new_car_matrix = matrix(FALSE, nrow = m, ncol = n)
  new_red_car = cbind(x, y)
  new_car_matrix[new_red_car] = TRUE
  new_car_matrix[bluecar] = TRUE
  
  return(list(new_red_car, new_car_matrix))
}

#blue car move
moveblue = function(bluecar, redcar, carmatrix){
  ncar = nrow(bluecar)
  x = bluecar[,1]
  y = bluecar[,2]
  m = nrow(carmatrix)
  n = ncol(carmatrix)
  
  nx = move_index(x + 1, m)
  next_bluecar = cbind(nx, y)
  ismoveok = carmatrix[next_bluecar]
  x = ifelse(ismoveok, x, nx)
  
  new_car_matrix = matrix(FALSE, nrow = m, ncol = n)
  new_blue_car = cbind(x, y)
  new_car_matrix[new_blue_car] = TRUE
  new_car_matrix[redcar] = TRUE
  
  return(list(new_blue_car, new_car_matrix))
}


#finish one step move by move red car one time and move blue car one time
onetimemove = function(car_matrix){
  
  old_car_matrix = car_matrix
  new_red_position = movered(car_matrix$redcar, car_matrix$bluecar, car_matrix$carmatrix)
  car_matrix$redcar = new_red_position[[1]]
  car_matrix$carmatrix = new_red_position[[2]]
  
  new_blue_position = moveblue(car_matrix$bluecar, car_matrix$redcar, car_matrix$carmatrix)
  car_matrix$bluecar = new_blue_position[[1]]
  car_matrix$carmatrix = new_blue_position[[2]]
  car_matrix$velocity = sum((car_matrix$carmatrix - old_car_matrix$carmatrix)>0)/(nrow(car_matrix$bluecar)
                                                                                  +nrow(car_matrix$redcar))
  return(car_matrix)
}


runBMLGrid = function(BMLGrid,numSteps) 
{ 
  if (numSteps<=0 | numSteps%%1 != 0){
    stop ('wrong number of steps')
  }
  
  for( i in 1:numSteps){
    BMLGrid = onetimemove(BMLGrid)}
  return (BMLGrid)
}

#3 Summary and Plot
#plot
rotate <- function(x) t(apply(x, 2, rev))


plot.BMLGrid = function(car_matrix){
  carmatrix = car_matrix$carmatrix
  redmatrix = matrix(0,nrow = nrow(carmatrix),ncol = ncol(carmatrix))
  bluematrix = matrix(0,nrow = nrow(carmatrix),ncol = ncol(carmatrix))
  redcar = car_matrix$redcar
  bluecar = car_matrix$bluecar
  
  redmatrix[redcar]=1
  bluematrix[bluecar]=2
  colormatrix=redmatrix+bluematrix
  image( rotate(colormatrix) , col = c( "white", "red", "blue"))
}

#summary
summary.BMLGrid = function(car_matrix){
  velocity = car_matrix$velocity
  row_dimension = nrow(car_matrix$carmatrix)
  col_dimension = ncol(car_matrix$carmatrix)
  num_of_blue = nrow(car_matrix$bluecar)
  num_of_red = nrow(car_matrix$redcar)
  information = list(velocity=velocity,row_dimension=row_dimension,
                     col_dimension=col_dimension,
                     num_of_blue=num_of_blue,num_of_red=num_of_red)
  return(information)
}

#4 Function for calculate velocity and code for analysis of Biham-Middleton-Levin Traffic Model
#calucate the average speed
averagevelocity = function(BMLGrid,numSteps)
{ record = rep(0,numSteps)
  for (i in 1:numSteps){
    BMLGrid = runBMLGrid(BMLGrid,1)
    record[i] = BMLGrid$velocity
  }
  return(sum(record)/numSteps)
}

#function for record velocity in each step
velocity_record = function(BMLGrid,numSteps)
{ record = rep(0,numSteps)
  for (i in 1:numSteps){
    BMLGrid = runBMLGrid(BMLGrid,1)
    record[i] = BMLGrid$velocity
  }
  return(record)
}

v = velocity_record(temp , 1000)
plot(v,pch = 20,cex = 0.1)

#100*100 1000 step image
c=c(0.2,0.3,0.4,0.5,0.6,0.7)
temp = lapply(c, function(c) createBMLGrid(100,100,c))

i = c(1:6)
grid = lapply(i , function(i) runBMLGrid(temp[[i]],1000))
plot(grid[[6]])

par(mfrow=c(2,3))
for (i in 1:6){
  main = paste0('Density ',as.character(c[i]))
  plot(grid[[i]])
}
#test the velocity under different density and grid shape
require(ggplot2)
require(reshape)
c=c(0.2,0.3,0.4,0.5,0.6,0.7)
temp = lapply(c, function(c) createBMLGrid(100,100,c))

i = c(1:6)
v=lapply(i , function(i) velocity_record(temp[[i]],1000))
v = sapply(i, function(i) rbind(v[[i]]))
colnames(v) = c('0.2','0.3','0.4','0.5','0.6','0.7')

q = as.data.frame(v)
q$time = 1:1000
q= q[seq(1,1000,2),]
q = melt(q, id = 'time', variable_name = "density")
colnames(q) = c('step', 'density', 'velocity')
ggplot(q, aes(step,velocity)) + geom_line(aes(colour = density))

c=c(0.2,0.3,0.4,0.5,0.6,0.7)
temp = lapply(c, function(c) createBMLGrid(100,200,c))

i = c(1:6)
v=lapply(i , function(i) velocity_record(temp[[i]],1000))
v = sapply(i, function(i) rbind(v[[i]]))
colnames(v) = c('0.2','0.3','0.4','0.5','0.6','0.7')

q = as.data.frame(v)
q$time = 1:1000
q= q[seq(1,1000,2),]
q = melt(q, id = 'time', variable_name = "density")
colnames(q) = c('step', 'density', 'velocity')
ggplot(q, aes(step,velocity)) + geom_line(aes(colour = density))

c=c(0.2,0.3,0.4,0.5,0.6,0.7)
temp = lapply(c, function(c) createBMLGrid(100,400,c))

i = c(1:6)
v=lapply(i , function(i) velocity_record(temp[[i]],1000))
v = sapply(i, function(i) rbind(v[[i]]))
colnames(v) = c('0.2','0.3','0.4','0.5','0.6','0.7')

q = as.data.frame(v)
q$time = 1:1000
q= q[seq(1,1000,2),]
q = melt(q, id = 'time', variable_name = "density")
colnames(q) = c('step', 'density', 'velocity')
ggplot(q, aes(step,velocity)) + geom_line(aes(colour = density))

#test velocity under different grid size
m = c(100,200,300,400)
temp = lapply(m, function(m) createBMLGrid(m,m,0.4))

i = c(1:4)
v=lapply(i , function(i) velocity_record(temp[[i]],1000))

par(mfrow=c(2,2))
for (i in 1:4){
  main = paste0('Grid Size: ',as.character(m[i]),'*',as.character(m[i]))
  plot(v[[i]],pch = 20,cex = 0.1,ylab='velocity',xlab='step',ylim=c(0,1),
       main = main)
}

#5 System time, Rfprof tool and Trace
temp = createBMLGrid(100,100,0.3)
redcar = temp$redcar
bluecar = temp$bluecar
carmatrix = temp$carmatrix

f = function() 
{
  runBMLGrid(temp,1000)
}
f()
system.time(runBMLGrid(temp,3000))
system.time(runBMLGrid(temp,5000))
system.time(runBMLGrid(temp,7000))

system.time(f( ))
Rprof("profile1.out")
system.time(
  f( )
)
Rprof(NULL)
p = summaryRprof("profile1.out")
p$by.self

##############################################################
#trace move blue
f = function() 
{
  moveblue(temp$bluecar,temp$redcar,temp$carmatrix)
}
Rprof("profile1.out")
system.time(
  f( )
)

Rprof(NULL)
p = summaryRprof("profile1.out")
p$by.self

trace("ifelse", quote(print(sys.calls())))
untrace("ifelse", quote(print(sys.calls())))
#6 Code efficiency under different situation
#the performance of code under different grid size
temp = list()
temp[[1]] = createBMLGrid(100,100,0.3)
temp[[2]] = createBMLGrid(200,100,0.3)
temp[[3]] = createBMLGrid(200,200,0.3)
temp[[4]] = createBMLGrid(400,400,0.3)

i = c(1:4)
time = lapply(i, function(i) system.time(runBMLGrid(temp[[i]],2000)))

# the performance of code under different density
c=c(0.2,0.3,0.4,0.5,0.6,0.7)
temp = lapply(c, function(c) createBMLGrid(100,100,c))

i = c(1:6)
time = lapply(i, function(i) system.time(runBMLGrid(temp[[i]],2000)))

#the performance of code under different number of simulations steps
temp = createBMLGrid(100,100,0.3)

i = seq(from=1000,to=7000,by=1000)
time = lapply(i, function(i) system.time(runBMLGrid(temp,i)))

#7 Package and Test
R code
package.skeleton(name="BML")
Shell code
R CMD build BML
R CMD INSTALL --build BML_1.0.tar.gz
R CMD check BML_1.0.tar.gz
Test
library(BML)
test = createBMLGrid(5,5,0.3)
plot(test)
i = c(1:4)
step = lapply(i,function(i)runBMLGrid(test,i))
par(mfrow=c(2,2))
for (i in 1:4) plot(step[[i]])

Package test
library(BML)
addition_test = function(){
  test = list()
  test$redcar = matrix(c(1,1,2,1,3,3),nrow = 3)
  test$bluecar = matrix(c(4,1,3,1,3,4),nrow = 3)
  test$carmatrix = matrix(FALSE,nrow=5,ncol=5)
  test$carmatrix[test$redcar] = TRUE
  test$carmatrix[test$bluecar] = TRUE
  
  result = list()
  result$redcar = matrix(c(1,1,2,2,4,4),nrow=3)
  result$bluecar = matrix(c(5,2,4,1,3,4),nrow=3)
  result$carmatrix = matrix(FALSE,nrow=5,ncol=5)
  result$carmatrix[result$redcar] = TRUE
  result$carmatrix[result$bluecar] = TRUE
  
  if (sum(runBMLGrid(test,1)$redcar == result$redcar)+
        sum(runBMLGrid(test,1)$bluecar == result$bluecar)+
        sum(runBMLGrid(test,1)$carmatrix == result$carmatrix) != 37)
    stop( 'Error in package')
}

addition_test()
