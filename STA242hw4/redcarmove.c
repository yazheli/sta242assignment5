#include<stdio.h>
#include<string.h>
#include <R.h>

void
redcarmove( int *grid, int *newgrid, const int *dims, int* numredcars, int *redcarlocations, int *velocity){
	
	int k = 0, i = 0 ,j = 0 ,ni = 0 ,nj = 0;
	int numMoved = 0;
	
	for (i = 0; i < dims[0] * dims[1]; i++) {
      newgrid[i] = grid[i];
   }
	for (k=0; k<numredcars[0];k++){
		i = redcarlocations[k] - 1;
		j = redcarlocations[k + numredcars[0]] - 1;
		ni = i;
		nj = j+1;
		if(nj == dims[1])
        nj = 0;
	
	if (grid[ni + nj * dims[0]] == 0){
		numMoved++;
        newgrid[ ni + nj * dims[0] ] = 1;
        newgrid[ i + j * dims[0] ] = 0;
		redcarlocations[k + numredcars[0]] = nj + 1;
	}
	velocity[0] = numMoved;

}
} 

